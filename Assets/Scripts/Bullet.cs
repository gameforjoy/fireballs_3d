using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _bounceForce;
    [SerializeField] private float _bounceRadius;

    private Vector3 _moveDirection;
    bool istrigger = false;
    private void Start()
    {
        _moveDirection = Vector3.forward;
    }

    private void Update()
    {
        transform.Translate(_moveDirection * _speed * Time.deltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Block block))
        {
            block.Break();
            Destroy(gameObject);
        }

        if (other.TryGetComponent(out Obstacle obstacle) && !istrigger)
        {
            //   Debug.Log("�������� ���������");
            istrigger = true;
         //   Bounce();
        }
    }
    private void FixedUpdate()
    {
        if (istrigger == true)
        {
            istrigger = false;
            Bounce();
        }
    }
    private void Bounce()
    {
      //  Vector3 moveDirection = Vector3.back + Vector3.up;
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.isKinematic = false;
     //   Debug.Log("�������� ���������");
        //rb.AddExplosionForce(_bounceForce, transform.position + new Vector3(0, -1f, 1f), _bounceRadius);
        rb.AddForce(new Vector3(0, 0.29f, -0.6f) * _bounceForce, ForceMode.Impulse);
        // istrigger = false;
    }
}
